import re
from urllib.parse import urlparse

from selenium.webdriver.chrome.options import Options

from crawler import logger
from crawler.database import get_database_connection


def is_valid_url(url):
    regex = re.compile(
        r'^(?:http|ftp)s?://'  # http:// or https://
        r'(?:(?:[A-Z0-9](?:[A-Z0-9-]{0,61}[A-Z0-9])?\.)+(?:[A-Z]{2,6}\.?|[A-Z0-9-]{2,}\.?)|'  # domain...
        r'localhost|'  # localhost...
        r'\d{1,3}\.\d{1,3}\.\d{1,3}\.\d{1,3})'  # ...or ip
        r'(?::\d+)?'  # optional port
        r'(?:/?|[/?]\S+)$', re.IGNORECASE)
    return re.match(regex, url) is not None


def get_domain(url, include_scheme):
    parsed_url = urlparse(url)
    if include_scheme:
        return '{url.scheme}://{url.netloc}/'.format(url=parsed_url)
    else:
        return parsed_url.netloc


def get_url_without_scheme(url):
    parsed_url = urlparse(url)
    return '{url.netloc}{url.path}'.format(url=parsed_url)


def get_url_without_parameters(url):
    parsed_url = urlparse(url)
    return '{url.scheme}://{url.netloc}{url.path}'.format(url=parsed_url)


def get_path(url):
    return urlparse(url).path


def is_url_gov_si(url):
    return urlparse(url).netloc.split('.')[-2:] == ['gov', 'si']


def get_disallowed_pages(robots_content):
    disallowed = []
    for line in robots_content.split('\n'):
        if line.startswith('Disallow'):
            line = line.split(' ')
            if len(line) == 2:
                disallowed.append(line[1])
    return disallowed


def get_chrome_options():
    chrome_options = Options()
    chrome_options.add_argument("--headless")
    chrome_options.add_argument("user-agent=fri-ieps-group10")
    chrome_options.add_experimental_option('excludeSwitches', ['enable-logging'])
    return chrome_options


def clean_database():
    conn = get_database_connection()
    with conn.cursor() as cur:
        cur.execute('''DELETE FROM crawldb.link''')
        cur.execute('''DELETE FROM crawldb.page_data''')
        cur.execute('''DELETE FROM crawldb.image''')
        cur.execute('''DELETE FROM crawldb.page''')
        cur.execute('''DELETE FROM crawldb.site''')
    logger.log('Clearing database.')
