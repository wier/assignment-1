from time import gmtime, strftime


def log(message):
    date_time = strftime("%Y-%m-%d %H:%M:%S", gmtime())
    print('[', date_time, '] ', message)
