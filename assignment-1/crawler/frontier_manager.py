import socket
from datetime import datetime

from crawler import logger, utils


class FrontierManager:
    TIMEOUT = 5

    frontier = []
    ip_addresses = dict()
    site_visits = dict()

    def __init__(self, seed_pages):
        self.frontier.extend(seed_pages)

    def page_in_frontier(self, url):
        print(self.frontier)
        return url in self.frontier

    def add_page(self, url):
        self.frontier.append(url)

    def get_next_page(self, lock):
        url = None
        with lock:
            while not url:
                for i in range(len(self.frontier)):
                    if not self.was_recently_visited(utils.get_domain(self.frontier[i], include_scheme=False)):
                        url = self.frontier.pop(i)
                        break

        url_without_scheme = utils.get_url_without_scheme(url)
        domain = utils.get_domain(url, include_scheme=True)
        domain_without_scheme = utils.get_domain(url, include_scheme=False)
        return url, url_without_scheme, domain, domain_without_scheme

    def is_empty(self):
        return len(self.frontier) == 0

    def get_domain_ip(self, domain):
        if self.ip_addresses.get(domain):
            return self.ip_addresses.get(domain)
        else:
            try:
                ip = socket.gethostbyname(domain)
                self.ip_addresses[domain] = ip
                return ip
            except socket.gaierror:
                logger.log("Couldn't get IP address of site {}".format(domain))
                return None

    def note_site_visit(self, domain, time):
        ip = self.get_domain_ip(domain)
        if ip:
            self.site_visits[ip] = time

    def update_visit_time(self, domain, time):
        ip = self.get_domain_ip(domain)
        if ip:
            self.site_visits[ip] = time

    def was_recently_visited(self, domain):
        ip = self.get_domain_ip(domain)
        if ip:
            current_time = datetime.now()
            if not self.site_visits.get(ip):
                self.note_site_visit(domain, current_time)
                return False
            else:
                if (current_time - self.site_visits.get(ip)).seconds >= self.TIMEOUT:
                    self.update_visit_time(domain, current_time)
                    return False
                else:
                    return True
        else:
            return False
