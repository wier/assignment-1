import concurrent.futures
import imghdr
import threading
import time
from collections import defaultdict
from urllib.parse import urljoin

import requests
from bs4 import BeautifulSoup
from selenium import webdriver

from crawler import database, logger, utils
from crawler.frontier_manager import FrontierManager


class Crawler:
    WEB_DRIVER = webdriver.Chrome('resources/chromedriver', options=utils.get_chrome_options())
    LOCK = threading.Lock()
    EXPECTED_FILE_EXTENSIONS = ['.pdf', '.doc', '.docx', '.ppt', '.pptx']
    IGNORED_FILE_EXTENSTION = ['.xls', '.xlsx', '.csv', '.ods', '.zip']

    in_links = defaultdict(int)

    def __init__(self, frontier_manager, max_workers, max_pages):
        self.frontier_manager = frontier_manager
        self.max_workers = max_workers
        self.max_pages = max_pages

    def get_robots_and_sitemap(self, domain):
        url = urljoin(domain, 'robots.txt')
        try:
            r = requests.get(url)
            if r.status_code < 400:
                robots = r.content.decode('utf-8')
                sitemap_index = robots.lower().find('sitemap')
                if sitemap_index != -1:
                    sitemap_url = robots[sitemap_index + 9:].split(' ')[0].strip()
                    r = requests.get(sitemap_url)
                    if r.status_code < 400:
                        sitemap = r.content.decode('utf-8')
                        return (robots, sitemap)
                else:
                    return (robots, None)
            else:
                logger.log("Site '{}' does not contain robots.txt file.".format(domain))
                return (None, None)
        except requests.exceptions.ConnectionError:
            logger.log("Could not connect to site '{}'".format(url))
            return (None, None)

    def get_html(self, url):
        with self.LOCK:
            logger.log("Scraping page {}".format(url))
            self.WEB_DRIVER.get(url)
            html_content = BeautifulSoup(self.WEB_DRIVER.page_source, 'html.parser')
            return html_content

    def populate_frontier(self, html, domain, page_id):
        for link in html.find_all('a', href=True):
            url = link.get('href')
            if not url:
                return
            if not 'http' in url:
                if '#' in url or 'javascript' in url:
                    continue
                else:
                    url = urljoin(domain, url)

            # strip url of parameters
            url = utils.get_url_without_parameters(url)

            with self.LOCK:
                # add new page to frontier if url is valid and is in .gov.si domain
                # and it is not already in frontier and wasn't previously scraped
                if utils.is_valid_url(url) \
                        and utils.is_url_gov_si(url) \
                        and not self.frontier_manager.page_in_frontier(url) \
                        and database.get_page_id(utils.get_url_without_scheme(url)) == -1:
                    self.frontier_manager.add_page(url)
                    self.in_links[utils.get_url_without_scheme(url)] = page_id

    def extract_images(self, html, domain):
        images = []
        for image in html.find_all('img'):
            data = b''
            if image.attrs.get('src'):
                url = image.attrs.get('src')
                if 'base64' in url:
                    # we do not save Base64 encoded images
                    continue
                elif 'http' not in url:
                    url = urljoin(domain, url)

                if utils.is_valid_url(url):
                    try:
                        r = requests.get(url)
                        if r.status_code < 400:
                            data = r.content
                    except requests.exceptions.ConnectionError:
                        logger.log("Could not get image at address {}".format(url))

            if data:
                # determine format and filename of image
                format = imghdr.what('', h=data)
                if image.attrs.get('alt'):
                    filename = image.attrs.get('alt')
                else:
                    filename = ''
                images.append((filename, format, data))

        return images

    def crawl(self):
        # wait for other threads to fill frontier
        if self.frontier_manager.is_empty():
            time.sleep(10)

        # get next page
        url, url_without_scheme, domain, domain_without_scheme = self.frontier_manager.get_next_page(self.LOCK)

        # return if page was visited in the past
        page_id = database.get_page_id(url_without_scheme)
        if page_id != -1:
            return

        with self.LOCK:
            # check if domain is already in database
            # if not, check for robots and sitemap and create a new entry
            site_id = database.get_site_id(domain_without_scheme)
            if site_id == -1:
                robots, sitemap = self.get_robots_and_sitemap(utils.get_domain(url, domain))
                site_id = database.save_site(domain_without_scheme, robots, sitemap)

        # get page which pointed to current one
        in_link = self.in_links.get(url_without_scheme)
        # do not scrape page if it is disallowed in robots file or if response code is not 2xx or 3xx
        robots_content = database.get_robots_content(site_id)
        if robots_content and utils.get_path(url) in utils.get_disallowed_pages(robots_content):
            return
        else:
            try:
                r = requests.head(url)
                if r.status_code > 400:
                    page_id = database.save_page(site_id, 'HTML', url_without_scheme, None, r.status_code)
                    if in_link:
                        database.save_link(in_link, page_id)
                    return
            except requests.exceptions.ConnectionError:
                logger.log("Could not connect to page '{}'".format(url))
                return

        # if url points to a binary file, ignore or save it depending on its type
        url_extension = url[url.rfind('.'):]
        if url_extension in self.IGNORED_FILE_EXTENSTION:
            return
        if url_extension in self.EXPECTED_FILE_EXTENSIONS:
            data = requests.get(url).content
            page_id = database.save_page(site_id, 'BINARY', url_without_scheme, None, r.status_code)
            if in_link:
                database.save_link(in_link, page_id)
            database.save_page_data(page_id, url_extension[1:].upper(), data)
        # if not, save page's HTML source
        else:
            html = self.get_html(url)
            page_id = database.save_page(site_id, 'HTML', url_without_scheme, html.prettify(), r.status_code)
            if in_link:
                database.save_link(in_link, page_id)

            # populate frontier with pages pointed to by current page
            self.populate_frontier(html, domain, page_id)

            # save images included in current page
            for image in self.extract_images(html, utils.get_domain(url, include_scheme=True)):
                database.save_image(page_id, image[0], image[1], image[2])

    def start(self):
        # utils.clean_database()

        # do one iteration of crawling so frontier gets filled up
        self.crawl()

        with concurrent.futures.ThreadPoolExecutor(max_workers=self.max_workers) as executor:
            for i in range(int(self.max_pages + 0.2 * self.max_pages)):
                future = executor.submit(self.crawl)
                # future.result()


seed_pages = ['http://evem.gov.si/', "https://gov.si", "https://e-uprava.gov.si", "https://e-prostor.gov.si"]
Crawler(FrontierManager(seed_pages), max_workers=4, max_pages=50000).start()
