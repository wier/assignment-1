from datetime import datetime

import psycopg2

from crawler import logger

DB_HOST = 'localhost'
DB_NAME = 'crawler'
DB_USER = 'postgres'
DB_PW = 'postgres'


def get_database_connection():
    conn = psycopg2.connect(host=DB_HOST, dbname=DB_NAME, user=DB_USER, password=DB_PW)
    conn.autocommit = True
    return conn


def get_next_id(cursor, sequence):
    cursor.execute('''SELECT NEXTVAL(%s)''', (sequence,))
    return cursor.fetchone()[0]


def get_site_id(url):
    conn = get_database_connection()
    with conn.cursor() as cur:
        cur.execute('''SELECT * FROM crawldb.site WHERE domain = %s''', (url,))
        sites = cur.fetchall()
        # logger.log('Getting site id; Executing sql: ' + cur.query.decode())
        if sites != []:
            return sites[0][0]
        else:
            return -1


def get_robots_content(site_id):
    conn = get_database_connection()
    with conn.cursor() as cur:
        cur.execute('''SELECT robots_content FROM crawldb.site WHERE id = %s''', (site_id,))
        return cur.fetchone()[0]


def save_site(domain, robots_content, sitemap_content):
    conn = get_database_connection()
    with conn.cursor() as cur:
        if sitemap_content:
            sitemap_content = sitemap_content.replace('\'', '\'\'')

        next_id = get_next_id(cur, 'crawldb.site_id_seq')
        cur.execute('''INSERT INTO crawldb.site VALUES(%s, %s, %s, %s) RETURNING id''',
                    (next_id, domain, robots_content, sitemap_content))
        # logger.log('Saving site; Executing sql: ' + cur.query.decode())
        return cur.fetchone()[0]


def get_pages_in_range(start_id, end_id):
    conn = get_database_connection()
    with conn.cursor() as cur:
        cur.execute('''SELECT * FROM crawldb.page WHERE page.id >= %s AND page.id < %s''', (start_id, end_id))
        return cur.fetchall()


def get_min_and_max_page_id():
    conn = get_database_connection()
    with conn.cursor() as cur:
        cur.execute('''SELECT MIN(page.id) FROM crawldb.page''')
        min_id = cur.fetchone()[0]
        cur.execute('''SELECT MAX(page.id) FROM crawldb.page''')
        max_id = cur.fetchone()[0]
        return min_id, max_id


def get_page_id(url):
    conn = get_database_connection()
    with conn.cursor() as cur:
        cur.execute('''SELECT * FROM crawldb.page WHERE url = %s''', (url,))
        # logger.log('Getting page id; Executing sql: ' + cur.query.decode())
        pages = cur.fetchall()
        if pages != []:
            return pages[0][0]
        else:
            return -1


def save_page(site_id, page_type_code, url, html_content, http_status_code):
    conn = get_database_connection()
    with conn.cursor() as cur:
        if html_content:
            html_content = html_content.replace('\'', '\'\'')

        next_id = get_next_id(cur, 'crawldb.page_id_seq')
        cur.execute('''INSERT INTO crawldb.page VALUES(%s, %s, %s, %s, %s, %s, %s) RETURNING id''',
                    (next_id, site_id, page_type_code, url, html_content, http_status_code, datetime.now()))
        # logger.log('Saving site; Executing sql: ' + cur.query.decode())
        return cur.fetchone()[0]


def count_pages():
    conn = get_database_connection()
    with conn.cursor() as cur:
        cur.execute('''SELECT COUNT(*) FROM crawldb.page''')
        return cur.fetchone()[0]


def save_image(page_id, filename, content_type, data):
    conn = get_database_connection()
    with conn.cursor() as cur:
        next_id = get_next_id(cur, 'crawldb.image_id_seq')
        cur.execute('''INSERT INTO crawldb.image VALUES(%s, %s, %s, %s, %s, %s)''',
                    (next_id, page_id, filename, content_type, data, datetime.now()))
        # logger.log('Saving image; Executing sql: ' + cur.query.decode())


def save_page_data(page_id, data_type_code, data):
    conn = get_database_connection()
    with conn.cursor() as cur:
        next_id = get_next_id(cur, 'crawldb.page_data_id_seq')
        cur.execute('''INSERT INTO crawldb.page_data VALUES(%s, %s, %s, %s)''',
                    (next_id, page_id, data_type_code, data))
        # logger.log('Saving page data; Executing sql: ' + cur.query.decode())


def get_link_id(from_page, to_page):
    conn = get_database_connection()
    with conn.cursor() as cur:
        cur.execute('''SELECT * FROM crawldb.link WHERE from_page = %s AND to_page = %s''', (from_page, to_page))
        # logger.log('Saving link; Executing sql: ' + cur.query.decode())
        links = cur.fetchone()
        if links:
            return links[0]
        else:
            return -1


def save_link(from_page, to_page):
    conn = get_database_connection()
    with conn.cursor() as cur:
        cur.execute('''INSERT INTO crawldb.link VALUES(%s, %s)''', (from_page, to_page))
        # logger.log('Saving link; Executing sql: ' + cur.query.decode())
