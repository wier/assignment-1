import csv
import math
import os
from collections import defaultdict

import matplotlib.pyplot as plt
import numpy as np

from crawler.database import get_pages_in_range, get_min_and_max_page_id

DATA_PATH = os.path.join(os.getcwd(), 'data')
STATUS_COUNTS_PATH = os.path.join(DATA_PATH, 'status_counts.csv')
DOMAIN_COUNTS_PATH = os.path.join(DATA_PATH, 'domain_counts.csv')
IMAGE_COUNTS_PATH = os.path.join(DATA_PATH, 'image_counts.csv')
IMAGE_FORMATS_PATH = os.path.join(DATA_PATH, 'image_formats.csv')


def page_batches():
    min_id, max_id = get_min_and_max_page_id()
    batch_size = 1000
    for i in range(min_id, max_id, batch_size):
        yield get_pages_in_range(i, i + batch_size)


def count_status_codes():
    status_counts = defaultdict(int)
    for batch in page_batches():
        for page in batch:
            status_counts[(math.floor(page[5] / 100)) * 100] += 1
    with open(STATUS_COUNTS_PATH, 'w') as f:
        writer = csv.writer(f)
        writer.writerow([100, 200, 300, 400, 500])
        writer.writerow(
            [status_counts[100], status_counts[200], status_counts[300], status_counts[400], status_counts[500]])


def count_domains():
    domain_counts = defaultdict(int)
    for batch in page_batches():
        for page in batch:
            domain_counts[page[3].split('/')[0]] += 1
    with open(DOMAIN_COUNTS_PATH, 'w') as f:
        writer = csv.writer(f)
        writer.writerow(['domain', 'pages'])
        for domain, pages in sorted(domain_counts.items(), key=lambda item: item[1], reverse=True):
            writer.writerow([domain, pages])


# status codes
if not os.path.isfile(STATUS_COUNTS_PATH):
    count_status_codes()
with open(STATUS_COUNTS_PATH, 'r') as f:
    reader = csv.reader(f)
    labels = next(reader)
    counts = [int(v) for v in next(reader)]
fig, ax = plt.subplots()
ax.bar(labels, counts)
ax.set_ylabel('No. of pages')
ax.set_xlabel('Status code')
plt.show()

# top domains by number of pages
if not os.path.isfile(DOMAIN_COUNTS_PATH):
    count_domains()
with open(DOMAIN_COUNTS_PATH, 'r') as f:
    reader = csv.reader(f)
    next(reader)
    labels, counts = [], []
    for i in range(5):
        row = next(reader)
        labels.append(row[0])
        counts.append(int(row[1]))
fig, ax = plt.subplots()
ax.bar(labels, counts)
ax.xaxis.set_tick_params(rotation=20)
ax.set_ylabel('No. of pages')
ax.set_xlabel('Domain')
plt.show()

# top domains by number of images
with open(IMAGE_COUNTS_PATH, 'r') as f:
    reader = csv.reader(f)
    next(reader)
    labels, counts = [], []
    for i in range(5):
        row = next(reader)
        labels.append(row[0])
        counts.append(int(row[1]))
fig, ax = plt.subplots()
ax.bar(labels, counts)
ax.set_xticklabels(labels, rotation=20)
ax.set_ylabel('No. of images')
ax.set_xlabel('Domain')
plt.show()

# image format distributions on top 5 domains by images
with open(IMAGE_FORMATS_PATH, 'r') as f:
    reader = csv.reader(f)
    labels = next(reader)[1:]
    domains, format_distributions = [], []
    for i in range(5):
        row = next(reader)
        domains.append(row[0])
        format_distributions.append([int(v) for v in row[1:]])
fig, ax = plt.subplots()
width = 0.1
leg = []
for i, fd in enumerate(format_distributions):
    p = ax.bar(np.arange(0, len(labels)) + i * width, fd, width=width)
    leg.append(p[0])
ax.set_xticks(np.arange(0, len(labels)) + width / 2)
ax.set_xticklabels(labels)
ax.legend(leg, domains)
plt.show()
