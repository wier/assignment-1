-- images per page
select COUNT(i.id) as img_count,
       p.id,
       MAX(p.url)  as url
from crawldb.page p,
     crawldb.image i
where p.id = i.page_id
  and p.url not like 'meteo.arso.gov.si%'
group by p.id
order by COUNT(i.id) desc;


-- images per site
select COUNT(i.id)   as img_count,
       s.id,
       MAX(s.domain) as url
from crawldb.page p,
     crawldb.image i,
     crawldb.site s
where s.id = p.site_id
  and p.id = i.page_id
group by s.id
order by COUNT(i.id) desc;

-- largest html content per page
-- does not finish
select *
from crawldb.page
order by html_content

-- number of file types per site (domain)
select s.domain    as url,
       count(i.id) as num,
       content_type
from crawldb.image i,
     crawldb.page p,
     crawldb.site s
where s.id = p.site_id
  and p.id = i.page_id
group by s.domain, content_type