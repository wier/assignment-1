import os
import time
from collections import defaultdict

from bs4 import BeautifulSoup
from tqdm import tqdm

import database
import utils
from data_processing import DataProcessor

DATA_DIRECTORY = os.path.join(os.getcwd(), 'data')
SURROUNDING_WORDS = 3
CHARACTER_LIMIT = 300


def extract_snippets(tokens, indices):
    snippets = []
    for index in indices:
        start = index - SURROUNDING_WORDS if index - SURROUNDING_WORDS >= 0 else 0
        end = index + SURROUNDING_WORDS + 1 if index + SURROUNDING_WORDS + 1 < len(tokens) else len(tokens) - 1
        snippets.append(' '.join(tokens[start:end]))
    return snippets


def print_results(query, time, frequencies, pages, snippets):
    print("Results for a query: \"{}\"\n".format(query))
    print("Results found in {:.0f} ms\n".format(time))
    print("{:<12} {:<40} {}".format('Frequencies', 'Document', 'Snippets (limited to {} characters)'.format(CHARACTER_LIMIT)))
    print("{} {} {}".format('-' * 12, '-' * 40, '-' * 80))
    for i in range(len(pages)):
        print("{:<12} {:<40} {}".format(frequencies[i], pages[i], ' ... '.join(snippets[i])[:CHARACTER_LIMIT]))
    print("-" * (12 + 40 + 80 + 3))


def sqlite_search(query):
    """Finds documents relevant to the given query using SQLite database as inverted index"""
    processor = DataProcessor(language='sl')
    processed_q, _ = processor.extract_lemmas_and_indices(query)

    # find documents and measure search time
    start_time = time.time()
    postings = database.find_postings_by_words(processed_q)

    search_time = (time.time() - start_time) * 1000
    pages, frequencies, snippets = [], [], []
    for p in postings[:20]:
        pages.append(p[0])
        frequencies.append(p[1])
        document = open(os.path.join(DATA_DIRECTORY, '.'.join(p[0].split('.')[:3]) + '/' + p[0]), 'r', encoding='utf-8')
        soup = BeautifulSoup(document, features='lxml')
        text = utils.remove_html_tags(soup.text)
        tokens = processor.tokenize(text)
        snippets.append(extract_snippets(tokens, p[2]))
    print_results(query, search_time, frequencies, pages, snippets)


def basic_search(query):
    """Finds documents relevant to the given query by searching all documents on demand"""
    processor = DataProcessor(language='sl')
    processed_q, _ = processor.tokenize(query, processing=True)

    # measure search time of finding occurences of query words in documents
    start_time = time.time()
    occurences = {}
    with tqdm(total=1416, desc='Searching pages') as progress:
        for site in os.listdir(DATA_DIRECTORY):
            if site.endswith('.gov.si'):
                site = os.path.join(DATA_DIRECTORY, site)
                for page in os.listdir(site):
                    if page.endswith('.html'):
                        text = utils.extract_text_from_page(os.path.join(site, page))
                        tokens, indices = processor.tokenize(text, processing=True)
                        for w in processed_q:
                            matching_i = [i for i, t in zip(indices, tokens) if t == w]
                            if matching_i:
                                if page in occurences:
                                    occurences[page].extend([i for i, t in zip(indices, tokens) if t == w])
                                else:
                                    occurences[page] = matching_i
                    progress.update()

    search_time = (time.time() - start_time) * 1000
    pages, frequencies, snippets = [], [], []
    for page, indices in sorted(occurences.items(), key=lambda item: len(item[1]), reverse=True)[:20]:
        pages.append(page)
        frequencies.append(len(indices))
        path = os.path.join('.'.join(page.split('.')[:-2]), page)
        text = utils.extract_text_from_page(os.path.join(DATA_DIRECTORY, path))
        tokens = processor.tokenize(text)
        snippets.append(extract_snippets(tokens, indices))
    print_results(query, search_time, frequencies, pages, snippets)
