import os

from tqdm import tqdm

import database
import utils
from data_processing import DataProcessor

data_directory = './data'
processor = DataProcessor(language='sl')

# extract lemmas and their indices from web pages in 'data' folder, create indexword and posting
# entries and save them to database
database.clean_database()
with tqdm(total=1416, desc='Processing data') as progress:
    indexwords = set()
    for site in os.listdir(data_directory):
        postings = []
        if site.endswith('.gov.si'):
            site = os.path.join(data_directory, site)
            for page in os.listdir(site):
                if page.endswith('.html'):
                    text = utils.extract_text_from_page(os.path.join(site, page))
                    words, indices = processor.extract_lemmas_and_indices(text)
                    for w in words:
                        postings.append((w, page, indices.get(w)))
                    indexwords = indexwords.union(words)
                    progress.update()
        database.insert_postings(postings)
    database.insert_words(indexwords)
