from collections import defaultdict

import nltk
import stanza
from nltk.corpus import stopwords


class DataProcessor:

    def __init__(self, language):
        self.nlp = stanza.Pipeline(lang=language, processors='tokenize,pos,lemma', tokenize_pretokenized=True,
                                   logging_level='WARN')
        self.batch_size = 2000
        self.stopwords_slovene = set(stopwords.words('slovene')).union(
            {'.', 'a', 'ali', 'ampak', 'april', 'avgust', 'biti', 'blizu', 'bolj', 'brez', 'c', 'cel', 'celo', 'd', 'da', 'daleč',
             'dan', 'danes', 'datum', 'december', 'del', 'deset', 'deseti', 'devet', 'deveti', 'do', 'dober', 'dobro', 'dokler',
             'dol', 'dolg', 'dovolj', 'drug', 'dva', 'e', 'eden', 'en', 'enkrat', 'etc', 'eur', 'f', 'februar', 'g', 'g.', 'ga.',
             'gor', 'gospa', 'gospod', 'halo', 'i', 'idr.', 'ii', 'iii', 'imeti', 'in', 'iti', 'iv', 'ix', 'iz', 'j', 'januar',
             'jaz', 'jesti', 'julij', 'junij', 'jutri', 'k', 'kadarkoli', 'kaj', 'kajti', 'kak', 'kako', 'kakor', 'kamor',
             'kamorkoli', 'kar', 'karkoli', 'kateri', 'katerikoli', 'kdaj', 'kdo', 'kdorkoli', 'ker', 'ki', 'kje', 'kjer',
             'kjerkoli', 'kljub', 'km', 'ko', 'koder', 'koderkol', 'kot', 'kratek', 'l', 'lahek', 'lahki', 'lahko', 'le', 'lep',
             'leto', 'm', 'maj', 'majhen', 'malce', 'malo', 'manj', 'marec', 'med', 'medtem', 'mesec', 'midev', 'mnogo', 'moj',
             'morati', 'morda', 'moči', 'n', 'na', 'nad', 'naj', 'najin', 'najmanj', 'največ', 'narobe', 'nato', 'nazaj', 'naš',
             'ne', 'nedavno', 'nedelje', 'nek', 'nekaj', 'nekateri', 'nekatero', 'nekdo', 'nekje', 'nekoč', 'nikamor', 'nikdar',
             'nikjer', 'nikoli', 'nič', 'nja', 'njegov', 'njen', 'njihov', 'njun', 'no', 'nocoj', 'nov', 'november', 'npr.', 'o',
             'ob', 'oba', 'oboj', 'od', 'odprt', 'okoli', 'oktober', 'on', 'oni', 'onidve', 'osem', 'osmi', 'oz.', 'p', 'pa',
             'pač', 'pet', 'petek', 'peti', 'po', 'pod', 'pogosto', 'poleg', 'poln', 'polno', 'ponavadi', 'ponedeljek', 'ponovno',
             'potem', 'povsod', 'pozdravljen', 'prav', 'pravi', 'prazen', 'prbl.', 'precej', 'pred', 'prej', 'prek', 'preko',
             'pri', 'pribl.', 'približno', 'primer', 'pripravljen', 'proti', 'prvi', 'r', 'ravno', 'redko', 'res', 'reča', 'saj',
             'sam', 'samo', 'se', 'sedaj', 'sedem', 'sedmi', 'september', 'seveda', 'sicer', 'skoraj', 'skozi', 'slab', 'sobota',
             'spet', 'sred', 'srednji', 'stran', 'stvar', 'svoj', 't', 'ta', 'tak', 'tako', 'takoj', 'tam', 'ter', 'težek',
             'težko', 'ti', 'tisti', 'tj.', 'tja', 'toda', 'torej', 'torek', 'tretji', 'trije', 'tu', 'tudi', 'tukaj', 'tvoj',
             'u', 'v', 'vaš', 'vedeti', 'vedno', 'velik', 'veliko', 'vendar', 'ves', 'več', 'vi', 'videv', 'viiok', 'visok',
             'vsaj', 'vsak', 'vsakdo', 'včasih', 'včeraj', 'www', 'x', 'z', 'za', 'zadaj', 'zadnji', 'zakaj', 'zaprt', 'zaradi',
             'zato', 'zdaj', 'zelo', 'znova', 'zunaj', 'č', 'če', 'često', 'četrtek', 'četrti', 'čez', 'čigav', 'š', 'še', 'šele',
             'šest', 'šesti', 'štirje', 'ž', 'že', 'gl.', 'gl', 'ipd.', 'ipd', 'itn.', 'itn', 'npr.', 'npr' '&lt', 'lt', '&gt',
             'gt'})
        self.special_characters = {"!", "\"", "\'\'", "#", "$", "%", "&", "\'", "(", ")", "*", "+", ",", "-", ".", "/", ":", ";",
                                   "<", "=", ">", "?", "@", "[", "\\", "]", "^", "_", "`", "{", "|", "}", "~", "--", "–", "✗",
                                   "·", "..."}

    def extract_lemmas_and_indices(self, text):
        """Lemmatizes and filters given text (html or query)"""
        text = self.preprocess_text(text)

        # tokenize text
        tokens = nltk.word_tokenize(text)

        # lemmatize text, filter lemmas and save their indices
        lemmas = set()
        indices = defaultdict(list)
        for i, word in enumerate(self.nlp([tokens[i:i + self.batch_size] for i in range(0, len(tokens), self.batch_size)]) \
                                         .iter_words()):
            lemma = word.lemma.strip()
            if not lemma in self.stopwords_slovene and not lemma in self.special_characters and word.upos != 'NUM':
                lemmas.add(lemma)
                indices[lemma].append(i)
        return lemmas, indices

    def tokenize(self, text, processing=False):
        """Tokenizes given text"""
        text = self.preprocess_text(text)
        tokens = nltk.word_tokenize(text)
        if processing:
            indices = [i for i, t in enumerate(tokens) if
                       not t.lower() in self.stopwords_slovene and not t in self.special_characters]
            tokens = [tokens[i] for i in indices]
            return tokens, indices
        else:
            return tokens

    def preprocess_text(self, text):
        """Prepares text for processing by removing unwanted characters"""
        return text.replace('\n', ' ').replace('\'', '')
