from bs4 import BeautifulSoup


def remove_html_tags(document):
    """Removes all content inside html tags"""
    filtered = []
    tag = False
    for char in document:
        if char == '<':
            tag = True
        if not tag:
            filtered.append(char)
        if char == '>':
            tag = False
    return ''.join(filtered)


def extract_text_from_page(path):
    html = open(path, 'r', encoding='utf-8')
    soup = BeautifulSoup(html, features='lxml')
    text = remove_html_tags(soup.text)
    return text
