import sqlite3

conn = sqlite3.connect('inverted-index.db')


def is_word_in_database(word):
    cur = conn.cursor()
    cur.execute('SELECT * FROM indexword WHERE word = \'{}\''.format(word))
    words = cur.fetchall()
    return words != []


def insert_words(words):
    cur = conn.cursor()
    for w in words:
        cur.execute('INSERT INTO indexword VALUES(\'{}\')'.format(w))
    conn.commit()


def insert_postings(postings):
    cur = conn.cursor()
    for p in postings:
        cur.execute('INSERT INTO posting VALUES(\'{}\', \'{}\', {}, \'{}\')'.format(p[0], p[1], len(p[2]),
                                                                                    ','.join([str(i) for i in p[2]])))
    conn.commit()


def find_postings_by_words(words):
    cur = conn.cursor()
    query = '''SELECT documentName, SUM(frequency) 'sum_frequency', GROUP_CONCAT(indexes) FROM posting
               WHERE {} GROUP BY documentName ORDER BY sum_frequency DESC''' \
        .format(' '.join(['word = \'{}\' OR'.format(w) for w in words])[:-3])
    cur.execute(query)
    return [[p[0], p[1], [int(i) for i in p[2].split(',')]] for p in cur.fetchall()]


def clean_database():
    cur = conn.cursor()
    cur.execute('DELETE FROM posting')
    cur.execute('DELETE FROM indexword')
    conn.commit()
