# WIER 2020 Lab Work

## Assignment 1: Web Crawler

Located in [assignment-1](assignment-1) folder

Python web crawler implementation. The crawler itself is contained within
[crawler](assignment-1/crawler) folder and can be run by executing
[web_crawler.py](assignment-1/crawler/web_crawler.py) file. Parameters such as 
maximum number of workers and expected number of pages to be scraped can be set
inside this file as well. Chromedrivers for MacOS and Windows operating systems,
needed by Selenium to render webpages, are provided in the 
[resources](assignment-1/crawler/resources) folder.

Some basic statistics are aggregated in [statistics](assignment-1/statistics) folder.
Running [statistics.py](assignment-1/statistics/statistics.py) produces several
charts, which can also be seen in the report.
[statistics_queries.sql](assignment-1/statistics/statistics_queries.sql) contains
some SQL queries which were used to obtain certain interesting information from
the database.

The file [db_without_blobs](assignment-1/db_without_blobs) is a backup of our
PostgreSQL database which was filled using our crawler implementation.
It does not contain binary files which were collected as well, but were omitted
for the goal of keeping the size of the backup small. The full backup is
available on request.

## Assignment 2: Structured Data Extraction

Implementation of three different methods of structured data extraction from
the web. They can be run with 
[run-extraction.py](implementation-extraction/run-extraction.py) with different
parameters:
- A: regular expression extraction
- B: XPath extraction
- C: automatic extraction using RoadRunner algorithm

The first two methods extract predefined data items from six example web pages
found in the [input-extraction](input-extraction) folder and output them in the
JSON format. The third method takes pairs of same pages and outputs a wrapper
for each of the pairs which can be used for extraction.

The implementation depends on the following libraries:
- [bs4](https://pypi.org/project/bs4/)
- [lxml](https://pypi.org/project/lxml/)

## Assignment 3:

Implementation of two different methods for document querying. Basic search is
performed by running [run-basic-search.py](implementation-indexing/run-basic-search.py)
script and adding the search term, e. g. `run-basic-search.py "social services"`.
The other implemented method performs search using an inverted index. The index is 
constructed by running [run-data-processing.py](implementation-indexing/run-data-processing.py) 
script. The search itself is performed by running 
[run-sqlite-search.py](implementation-indexing/run-sqlite-search.py) script and adding
the search term the same way as with the first method. The inverted index constructed
from web page documents in folder [data](implementation-indexing/data) is available
as an SQLite database file [inverted-index.db](implementation-indexing/inverted-index.db).

Implementations depend on the following libraries:
- [bs4](https://pypi.org/project/bs4/)
- [lxml](https://pypi.org/project/lxml/)
- [nltk](https://pypi.org/project/nltk/)
- [stanza](https://pypi.org/project/stanza/)
- [tqdm](https://pypi.org/project/tqdm/)

