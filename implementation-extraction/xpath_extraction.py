from lxml import html
import re

def extract_data_records(page, site):
    if site == 'overstock':
        return extract_from_overstock(page)
    elif site == 'rtvslo':
        return extract_from_rtvslo(page)
    elif site == 'musicbrainz':
        return extract_from_musicbrainz(page)
    else:
        raise ValueError("Unsupported site: {}".format(page))


def extract_from_musicbrainz(page):
    item = {}
    albums = []
    tree = html.fromstring(page)

    # Description
    description = ''
    description_raw = tree.xpath('//*[@id="content"]/div[4]/div/p//text()')
    for elt in description_raw:
        processed_elt = elt.replace('\n', ' ')
        processed_elt = re.sub(r' +', ' ', processed_elt)
        description += processed_elt

    # Discography
    num_tables = len(tree.xpath('//*[@id="content"]/form/table'))
    num_table_rows = 0

    # loop through all tables
    #   loop through each row in every table
    #       process each row
    for i in range(1, num_tables+1):
        num_table_rows = len(tree.xpath('//*[@id="content"]/form/table['+str(i)+']/tbody/tr'))
        for j in range(1, num_table_rows+1):
            albums.append(process_table_row(tree, i, j))

    item['Description'] = description
    item['Discography'] = albums

    return item


# Helper function
def process_table_row(tree, i, j):
    item = {}

    # YEAR, TITLE, RELEASES
    year = tree.xpath('//*[@id="content"]/form/table['+str(i)+']/tbody/tr['+str(j)+']/td[1]/text()')[0]
    title = tree.xpath('//*[@id="content"]/form/table['+str(i)+']/tbody/tr['+str(j)+']/td[2]/a/bdi/text()')[0]
    releases = tree.xpath('//*[@id="content"]/form/table[' + str(i) + ']/tbody/tr[' + str(j) + ']/td[5]/text()')[0]

    # ARTIST
    artists_raw = tree.xpath('//*[@id="content"]/form/table['+str(i)+']/tbody/tr['+str(j)+']/td[3]/span')
    artists = ''

    # multiple artists
    if len(artists_raw) > 0:
        for k in range(1, len(artists_raw)+1):
            artist = tree.xpath('//*[@id="content"]/form/table['+str(i)+']/tbody/tr['+str(j)+']/td[3]/span['+str(k)+']/a/bdi/text()')[0]
            artists += artist + ', '
        artists = artists[:-2]
    # one artist
    else:
        artists = tree.xpath('//*[@id="content"]/form/table['+str(i)+']/tbody/tr['+str(j)+']/td[3]/a/bdi/text()')[0]

    # thomas vs daft_punk
    # '//*[@id="content"]/form/table[2]/tbody/tr/td[3]/span[1]/a/bdi'
    # '//*[@id="content"]/form/table[8]/tbody/tr[18]/td[3]/a[1]/bdi'

    # RATING
    rating = tree.xpath('//*[@id="content"]/form/table['+str(i)+']/tbody/tr['+str(j)+']/td[4]/span/span/span/text()')
    # no rating assigned
    if len(rating) == 0:
        rating = 'N/A'
    # rating assigned
    else:
        rating = rating[0]

    item['Year'] = year
    item['Title'] = title
    item['Artist'] = artists
    item['Rating'] = rating
    item['Releases'] = releases

    # print(item)

    return item


def extract_from_overstock(page):
    tree = html.fromstring(page)
    items = []

    elts = int(len(tree.xpath('/html/body/table[2]/tbody/tr[1]/td[5]/table/tbody/tr[2]/td/table/tbody/tr/td/table/tbody/tr')))

    for i in range(1, elts+1):
        item = process_overstock_element(tree, i)
        if item:
            items.append(item)

    return items


# Helper function
def process_overstock_element(tree, index):
    elts = len(tree.xpath('/html/body/table[2]/tbody/tr[1]/td[5]/table/tbody/tr[2]/td/table/tbody/tr/td/table/tbody/tr[' +
        str(index) + ']/td'))

    # empty table elements for spacing, skip
    if elts == 1:
        return

    title = tree.xpath(
        '/html/body/table[2]/tbody/tr[1]/td[5]/table/tbody/tr[2]/td/table/tbody/tr/td/table/tbody/tr[' +
        str(index) + ']/td[2]/a/b/text()')[0]
    listpr = tree.xpath(
        '/html/body/table[2]/tbody/tr[1]/td[5]/table/tbody/tr[2]/td/table/tbody/tr/td/table/tbody/tr[' +
        str(index) + ']/td[2]/table/tbody/tr/td[1]/table/tbody/tr[1]/td[2]/s/text()')[0]
    price = tree.xpath(
        '/html/body/table[2]/tbody/tr[1]/td[5]/table/tbody/tr[2]/td/table/tbody/tr/td/table/tbody/tr[' +
        str(index) + ']/td[2]/table/tbody/tr/td[1]/table/tbody/tr[2]/td[2]/span/b/text()')[0]
    yousave = tree.xpath(
        '/html/body/table[2]/tbody/tr[1]/td[5]/table/tbody/tr[2]/td/table/tbody/tr/td/table/tbody/tr[' +
        str(index) + ']/td[2]/table/tbody/tr/td[1]/table/tbody/tr[3]/td[2]/span/text()')[0]
    content = tree.xpath(
        '/html/body/table[2]/tbody/tr[1]/td[5]/table/tbody/tr[2]/td/table/tbody/tr/td/table/tbody/tr[' +
        str(index) + ']/td[2]/table/tbody/tr/td[2]/span/text()')[0]

    # parse saving and savingpercent (it's a single string)
    tokens = yousave.split(' ')
    saving = tokens[0]
    savingpercent = tokens[1][1: -1]

    item = {}
    item['Title'] = title
    item['List price'] = listpr
    item['Price'] = price
    item['Saving'] = saving
    item['Saving percent'] = savingpercent
    item['Content'] = content

    # print(item)

    return item

def extract_from_rtvslo(page):
    items = {}
    tree = html.fromstring(page)

    # Author
    items['Author'] = tree.xpath('//*[@id="main-container"]/div[3]/div/div[1]/div[1]/div/text()')[0]

    # Published time
    publishedtime_text = tree.xpath('//*[@id="main-container"]/div[3]/div/div[1]/div[2]/text()')[0]
    tokens = publishedtime_text.split(' ')
    items['PublishedTime'] = tokens[1] + ' ' + tokens[2] + ' ' + tokens[3] + ' ' + tokens[4]

    # Title
    items['Title'] = tree.xpath('//*[@id="main-container"]/div[3]/div/header/h1/text()')[0]

    # SubTitle
    items['SubTitle'] = tree.xpath('//*[@id="main-container"]/div[3]/div/header/div[2]/text()')[0]

    # Lead
    items['Lead'] = tree.xpath('//*[@id="main-container"]/div[3]/div/header/p/text()')[0]

    # Content
    # TODO: get different elements
    content = tree.xpath('//*[@id="main-container"]/div[3]/div/div[2]')[0]

    # selects all text
    content_text = tree.xpath('//*[@id="main-container"]/div[3]/div/div[2]/article/p//text()')
    # print(content_text)

    # selects video
    content_video = tree.xpath('//*[@id="main-container"]/div[3]/div/div[2]/article/p[6]/iframe/@src')
    # print(content_video)

    # TODO:
    # advertisement ?

    # items['Content'] = [content_text, content_video]
    items['Content'] = content_text

    # DEBUG
    # print('Author: ', items['Author'])
    # print('PublishedTime:', items['PublishedTime'])
    # print('Title:', items['Title'])
    # print('SubTitle:', items['SubTitle'])
    # print('Lead:', items['Lead'])
    # print('Content:', items['Content'])

    return items
