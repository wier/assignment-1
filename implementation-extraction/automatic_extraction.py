from bs4 import BeautifulSoup, Comment


def preprocess(soup):
    """Removing unnecessary tags and attributes from a HTML page

    soup -- BeautifulSoup object of a HTML page
    """

    for s in soup.findAll(text=lambda text: isinstance(text, Comment)):
        s.extract()

    for s in soup.findAll(['head', 'script', 'style', 'circle']):
        s.extract()

    for s in soup.find_all():
        s.attrs = {}

    page = soup.prettify().split('\n')

    formatting_tags = ["<b>", "<strong>", "<i>", "<em>", "<mark>", "<small>", "<del>", "<ins>", "<sub>", "<sup>",
                       "</b>", "</strong>", "</i>", "</em>", "</mark>", "</small>", "/<del>", "</ins>", "</sub>",
                       "</sup>", "<div>", "</div>", "<span>", "</span>"]

    page = [tag for tag in page if tag.strip(" ") not in formatting_tags
                                and (tag.startswith("<") or tag.startswith(" "))]

    return page


def square_match(page, square, index):
    """Compare a square to a upward portion of the page in reverse order."""
    for i in range(len(page[index:index + len(square)])):
        if not (page[index + i] == square[i] or square[i] == "#PCDATA" and not page[index + i].strip(" ").startswith("<")):
            return False
    return True


def mismatch(i, j, page1, page2):
    """Generalize the mismatch in the wrapper."""
    terminal_tag = page1[i - 1]
    square = {page1[i]: None,
              page2[j]: None}

    for page, index in [(page1, i), (page2, j)]:
        # Find square candidates
        if terminal_tag not in page[index:]:
            continue
        else:
            next_terminal_tag = page.index(terminal_tag, index)
            square[page[index]] = page[index:next_terminal_tag + 1]

        # Check if square candidate matches previous lines
        for k in range(1, len(square[page[index]]) + 1):
            if square[page[index]] is None:
                break
            if page[index - k] != square[page[index]][-k]:
                if page[index - k] == "#PCDATA" and not square[page[index]][-k].strip(" ").startswith("<"):
                    square[page[index]][-k] = "#PCDATA"
                    continue
                else:
                    square[page[index]] = None
                break

    if square[page1[i]] is None and square[page2[j]] is None:
        # Mismatch is not due to an iterator, so check for optional
        for k in range(1, min(len(page1) - i - 1, len(page2) - j - 1)):
            p1 = page1[i+k]
            p2 = page2[j]
            if page1[i].strip(" ") == page2[j + k].strip(" "):
                page2[j] = "(" + page2[j]
                page2[j + k - 1] += " )?"
                page1[i:i] = page2[j:j + k]
                return [i + k, page1, j + k, page2]
            elif page1[i + k].strip(" ") == page2[j].strip(" "):
                page1[i] = "(" + page1[i]
                page1[i + k - 1] += " )?"
                page2[j:j] = page2[i:i + k]
                return [i + k, page1, j + k, page2]
            elif not page1[i].strip(" ").startswith("<") and not page2[j + k].strip(" ").startswith("<"):
                page2[j] = "(" + page2[j]
                page2[j + k - 1] += " )?"
                page1[i:i] = page2[j:j + k]
                page1[i] = "#PCDATA"
                page2[j + k] = "#PCDATA"
                return [i + k, page1, j + k, page2]
            elif not page1[i + k].strip(" ").startswith("<") and not page2[j].strip(" ").startswith("<"):
                page1[i] = "(" + page1[i]
                page1[i + k - 1] += " )?"
                page2[j:j] = page2[i:i + k]
                page1[i + k] = "#PCDATA"
                page2[j] = "#PCDATA"
                return [i + k, page1, j + k, page2]
        return [i+1, page1, j+1, page2]

    if square[page1[i]] is None:
        square = square[page2[j]]
    else:
        square = square[page1[i]]
    new_data = []
    for page, index in [(page1, i), (page2, j)]:
        # Find all occurrences of the square and replace them in the wrapper
        start = index - len(square)
        square_match(page, square, start)
        while start >= 0:
            if square_match(page, square, start):
                start -= len(square)
            else:
                start += len(square)
                break

        end = index
        while end <= len(page) and square_match(page, square, end):
            end += len(square)

        page[start:end] = square
        page[start] = "(" + page[start]
        page[start + len(square) - 1] = page[start + len(square) - 1] + " )+"
        new_data.append(start + len(square))
        new_data.append(page)

    return new_data


def match(page1, page2, i=0, j=0):
    if page1[0] != page2[0]:
        return page1
    while i < len(page1) and j < len(page2):
        if page1[i].strip(" ") == page2[j].strip(" "):
            i += 1
            j += 1
        elif not page1[i].strip(" ").startswith("<") and not page2[j].strip(" ").startswith("<"):
            page1[i] = "#PCDATA"
            page2[j] = "#PCDATA"
            i += 1
            j += 1
        else:
            [i, page1, j, page2] = mismatch(i, j, page1, page2)

    return page1


def extract_data_records(page1, page2):
    soup1 = preprocess(BeautifulSoup(page1, features="html.parser"))
    soup2 = preprocess(BeautifulSoup(page2, features="html.parser"))

    wrapper = match(soup1, soup2)
    return wrapper
