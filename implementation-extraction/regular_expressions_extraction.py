import html
import re
from collections import defaultdict

from bs4 import BeautifulSoup


def extract_data_records(page, site):
    if site == 'overstock':
        return extract_from_overstock(page)
    elif site == 'rtvslo':
        return extract_from_rtvslo(page)
    elif site == 'musicbrainz':
        return extract_from_musicbrainz(page)
    else:
        raise ValueError("Unsupported site: {}".format(site))


def extract_from_overstock(page):
    """Return data extracted from given Overstock page"""
    items_list = defaultdict(list)
    regex = r'<tr bgcolor="#[df]{6}"> \n<td valign="top" align="center">(?:.|\n)*?<a href=".*"><b>(.*)</b></a>(?:.|\n)*?List Price:(?:.|\n)*?\$((?:\d|,)+\.\d\d)(?:.|\n)*?Price:(?:.|\n)*?\$((?:\d|,)+\.\d\d)(?:.|\n)*?You Save:(?:.|\n)*?\$((?:\d|,)+\.\d\d) \((\d+)%\)(?:.|\n)*?<span class="normal">((?:.|\n)*?)<br><a(?:.|\n)*?</tr>'
    matcher = re.compile(regex)
    for data_item in re.finditer(matcher, page):
        item = defaultdict(str)
        item['Title'] = data_item.group(1)
        item['ListPrice'] = data_item.group(2)
        item['Price'] = data_item.group(3)
        item['Saving'] = data_item.group(4)
        item['SavingPercent'] = data_item.group(5)
        item['Content'] = data_item.group(6).strip().replace('\n', ' ')
        items_list['Items'].append(item)
    return items_list


def extract_from_rtvslo(page):
    """Return data extracted from given RTVSLO article"""
    item = defaultdict(str)
    item['Author'] = re.search(r'<div class="author-name">(.*?)</div>', page).group(1)
    item['PublishedTime'] = re.search(r'<div class="publish-meta">(.*?)<br>', page, re.DOTALL).group(1).strip()
    item['Title'] = re.search(r'<header class="article-header">.*?<h1>(.*?)</h1>', page, re.DOTALL).group(1)
    item['SubTitle'] = re.search(r'<header class="article-header">.*?<div class="subtitle">(.*?)</div>', page,
                                 re.DOTALL).group(1)
    content_pattern = r'<div class="article-body">.*?' \
                      r'<article class="article">(.*?)</article>.*?' \
                      r'</div>'
    content = BeautifulSoup(re.search(content_pattern, page, re.DOTALL).group(1), features='lxml')
    for p in content.find_all('p'):
        stripped = p.text.strip()
        if stripped:
            item['Content'] += stripped + ' '
    item['Content'] = item['Content'][:-1]
    return item


def extract_from_musicbrainz(page):
    """Return data extracted from given MusicBrainz artist page"""
    item = {}

    # extract artist description from top of the page
    description_pattern = r'<div class="wikipedia-extract">.*?' \
                          r'<div class="wikipedia-extract-body.*?>' \
                          r'(.*?)' \
                          r'</div>'
    description = BeautifulSoup(re.search(description_pattern, page, re.DOTALL).group(1), features='lxml')
    item['Description'] = ' '.join([l.strip() for l in description.text.split('\n') if l.strip()])

    # extract artist's discography
    item['Discography'] = []
    record_pattern = r'<tr class="(?:odd|even)">.*?' \
                     r'<td class="c">(\d{4})</td>.*?' \
                     r'<td>.*?<a href=".*?">.*?<bdi>(.*?)</bdi>.*?' \
                     r'<td>((?:<span class="name-variation">)?<a.*?href=".*?".*?title=".*?">.*?<bdi>.*?</bdi>.*?)' \
                     r'<td class="c"><span class="inline-rating"><span class="star-rating" tabindex="-1">' \
                     r'(?:<span.*?class="current-rating".*?>(\d\.?\d*))?.*?' \
                     r'<td class="c">(\d+)'
    for match in re.finditer(record_pattern, page, re.DOTALL):
        recording = defaultdict(str)
        recording['Year'] = match.group(1)
        recording['Title'] = html.unescape(match.group(2))
        recording['Artist'] = ' '.join(
            [l.strip() for l in BeautifulSoup(match.group(3), features='lxml').text.strip().split('\n')])
        recording['Rating'] = match.group(4) if match.group(4) else 'N/A'
        recording['Releases'] = match.group(5)
        item['Discography'].append(recording)
    return item
