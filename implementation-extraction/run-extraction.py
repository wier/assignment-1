import json
import sys

import automatic_extraction
import regular_expressions_extraction
import xpath_extraction

# check if appropriate number of arguments were given
if len(sys.argv) != 2:
    raise ValueError("Wrong number of arguments were given")

# extract web page HTMLs
with open('../input-extraction/overstock.com/jewelry01.html', 'r', encoding='cp1252') as f:
    jewelry01 = f.read()
with open('../input-extraction/overstock.com/jewelry02.html', 'r', encoding='cp1252') as f:
    jewelry02 = f.read()
with open('../input-extraction/rtvslo.si/Audi A6 50 TDI quattro_ nemir v premijskem razredu - RTVSLO.si.html', 'r',
          encoding='utf-8') as f:
    audi = f.read()
with open('../input-extraction/rtvslo.si/Volvo XC 40 D4 AWD momentum_ suvereno med najboljše v razredu - RTVSLO.si.html', 'r',
          encoding='utf-8') as f:
    volvo = f.read()
with open('../input-extraction/musicbrainz.org/Daft Punk - MusicBrainz.html', 'r', encoding='utf-8') as f:
    daft_punk = f.read()
with open('../input-extraction/musicbrainz.org/Luke Howard - MusicBrainz.html', 'r', encoding='utf-8') as f:
    luke_howard = f.read()

# extract data records from web pages using specified method and output it to standard output
if sys.argv[1] == 'A':
    print("Extracting data using regular expressions:")
    print(json.dumps(regular_expressions_extraction.extract_data_records(jewelry01, 'overstock'), ensure_ascii=False))
    print(json.dumps(regular_expressions_extraction.extract_data_records(jewelry02, 'overstock'), ensure_ascii=False))
    print(json.dumps(regular_expressions_extraction.extract_data_records(audi, 'rtvslo'), ensure_ascii=False))
    print(json.dumps(regular_expressions_extraction.extract_data_records(volvo, 'rtvslo'), ensure_ascii=False))
    print(json.dumps(regular_expressions_extraction.extract_data_records(daft_punk, 'musicbrainz'), ensure_ascii=False))
    print(json.dumps(regular_expressions_extraction.extract_data_records(luke_howard, 'musicbrainz'), ensure_ascii=False))
elif sys.argv[1] == 'B':
    print("Extracting data using XPath:")
    print(json.dumps(xpath_extraction.extract_data_records(jewelry01, 'overstock'), ensure_ascii=False))
    print(json.dumps(xpath_extraction.extract_data_records(jewelry02, 'overstock'), ensure_ascii=False))
    print(json.dumps(xpath_extraction.extract_data_records(audi, 'rtvslo'), ensure_ascii=False))
    print(json.dumps(xpath_extraction.extract_data_records(volvo, 'rtvslo'), ensure_ascii=False))
    print(json.dumps(xpath_extraction.extract_data_records(daft_punk, 'musicbrainz'), ensure_ascii=False))
    print(json.dumps(xpath_extraction.extract_data_records(luke_howard, 'musicbrainz'), ensure_ascii=False))
elif sys.argv[1] == 'C':
    print("Extracting data using automatic extraction:")
    print(automatic_extraction.extract_data_records(jewelry01, jewelry02))
    print(automatic_extraction.extract_data_records(audi, volvo))
    print(automatic_extraction.extract_data_records(daft_punk, luke_howard))
else:
    raise ValueError("Given argument does not correspond to any supported web extraction function")
